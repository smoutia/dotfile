return {
  "nvim-neotest/neotest",
  ft = { "python" },
  dependencies = {
    "nvim-neotest/neotest-python",
  },
  opts = function()
    return {
      -- your neotest config here
      adapters = {
        require("neotest-python"),
      },
    }
  end,
  config = function(_, opts)
    require("neotest").setup(opts)
  end,
}
